class Gigasecond
  def self.from(t)
    t + 1000000000.seconds
  end
end
