class RnaComplement
  @@table =  { 'G' => 'C', 'C' => 'G', 'T' => 'A', 'A' => 'U' }
  def self.of_dna(str)
    str.chars.map{ |c| @@table[c] }.join("")
  end
end
