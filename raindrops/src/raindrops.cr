class Raindrops
  def self.drops(num)
    str = ""
    str = str + "Pling" if (num % 3).zero?
    str = str + "Plang" if (num % 5).zero?
    str = str + "Plong" if (num % 7).zero?
    return num.to_s if str.blank?
    str
  end
end
