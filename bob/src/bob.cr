class Bob
  def self.hey(str)
    return "Fine. Be that way!" if str.strip == ""
    isCaps = str == str.upcase
    isNocaps = str == str.downcase
    return "Whoa, chill out!" if isCaps && !isNocaps
    return "Sure." unless (/.*\?$/ =~ str).nil?
    "Whatever."
  end
end
