# Please implement your solution to hamming in this file
class Hamming
  def self.compute(str1, str2)
    raise ArgumentError.new("strands aren't of equal length") unless str1.size == str2.size
    count = 0
    i = 0
    while i < str1.size
      count = count + 1 unless str1[i] == str2[i]
      i = i + 1
    end
    count
  end
end
